# -*- mode: python ; coding: utf-8 -*-


a = Analysis(
    ['../muphyn/__main__.py'],
    pathex=[],
    binaries=[],
    datas=[('../muphyn/assets', 'muphyn/assets'),
        ('../muphyn/docs', 'muphyn/docs'),
        ('../muphyn/libraries', 'muphyn/libraries'),
        ('../muphyn/packages', 'muphyn/packages'),
        ('../muphyn/themes', 'muphyn/themes'),
        ('../muphyn/utils', 'muphyn/utils') 
    ],
    hiddenimports=[],
    hookspath=[],
    hooksconfig={},
    runtime_hooks=[],
    excludes=[],
    noarchive=False,
    optimize=0,
)
pyz = PYZ(a.pure)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name='MuPhyN',
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
    disable_windowed_traceback=False,
    argv_emulation=False,
    target_arch=None,
    codesign_identity=None,
    entitlements_file=None,
    icon='../muphyn/assets/MuPhyN.ico',
)

coll = COLLECT(
    exe,
    a.binaries,
    a.datas,
    strip=False,
    upx=True,
    upx_exclude=[],
    name='MuPhyN',
)
