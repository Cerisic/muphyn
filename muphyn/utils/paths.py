from os.path import dirname, abspath

ROOT_DIR = dirname(dirname(abspath(__file__)))
THEME_PATH = ROOT_DIR + "/themes/light.qss"