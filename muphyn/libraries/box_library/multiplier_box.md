# Multiplier box

## 1. Description
This allows user to product the output of 2 or more boxes. 

At each step of the simulation will calculate the product of value of all input signals.

## 2. Parameters
Multiplier box does not have any parameter.

## 3. IO
|       | Type | Default | Minimum | Maximum |
| ---   |---  |---| --- | --- |
|Inputs |Unlimited|2| 1 | / |
|Outputs|Fixed|1| / | / |