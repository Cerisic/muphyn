from enum import Enum
class PhysicalQuantityType(Enum):
    ELECTRIC = 1
    THERMAL = 2
    LIQUID_FLOW = 3

