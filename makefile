default: wheel

portable:
	pyinstaller dist/MuPhyN.spec --workpath=dist/build --distpath=dist/portable

wheel:
	python3 -m build

upload: wheel
	python3 -m twine upload --repository pypi dist/*