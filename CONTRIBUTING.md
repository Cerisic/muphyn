# Contributing

## Discuss First
When contributing to this repository, please first open an issue in order to discuss the change you wish to make with the maintainers of this repository. That should avoid spending time on a change that will not be accepted in the project.

## Pull Request Process

1. Fork the project to your personnal gitlab account
2. Create a branch for your modification (one branch per change or fix in the project)
3. Create a Pull Request that will need to be checked and accepted by a maintainer

## Code Convention
